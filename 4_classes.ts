/**Readonly */
class Auto {
  readonly model: string;

  constructor(model: string) {
    this.model = model
  }

  getModel() {
    return this.model
  }

  // setModel(value: string) {
  //   this.model = this.model
  // }
}

/** Наследование и модификаторы доступа */
class Animal {
  protected voice: string = ''
  public color: string = 'black'

  constructor() {
    this.go()
  }

  private go() {
    console.log('Go')
  }
}

class Cat extends Animal {
  public setVoice(voice: string): void {
    this.voice = voice
  }
}

const animal = new Animal()
// animal.go()

const cat = new Cat()
cat.setVoice('test')

abstract class Component {
  abstract render(): void
  abstract info(): string

  remove(): void {
    console.log('remove')
  }
}

class AppComponent extends Component {
  render(): void {
    console.log('Component on render')
  }

  info(): string {
    return 'This is info';
  }
}

const component = new AppComponent()
component.info()
component.remove()
component.render()
