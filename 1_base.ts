/**
 * Базовые типы
 * string
 * number
 * boolean
 * undefined
 * null
 */
const firstname: string = 'Anna'

const age: number = 20
const height: number = 165.5
const selfConceitLevel: number = 10e100

const isEmployee: boolean = false

const nullable: null = null;
const undefinedVar: undefined = undefined

/** Массивы */
const numberArray: number[] = [1, 1, 3, 5, 8, 13]
const stringArray: Array<string> = ['Anna', 'Pavel', 'Oleg']

const mixedArray: [string, number, boolean] = ['string', 123, false]

/** Any */
let obscureVar: any

obscureVar = 'string'
obscureVar = 123
obscureVar = [1, 2, 3]

/**Type */

type UserName = string

const userName: UserName = 'admin'

type Inn = string | number
const firstInn: Inn = 1234
const secondInn: Inn = '1234'

type Login = number | 'firstUser' | 'secondUser' | null | undefined
const login1: Login = "firstUser"
const login2: Login = 12321
const login3: Login = undefined
