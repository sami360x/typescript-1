type Coords = {
  x: number,
  y: number
}

interface Circle {
  coords: Coords;
  color?: string;
  radius: number;
}

const circle: Circle = {
  coords: {
    x: 1,
    y: 0,
  },
  radius: 10,
}

const circleRed: Circle = {
  coords: {
    x: 0,
    y: 0,
  },
  radius: 1,
  color: 'red',
}

circle.color = 'grey'

interface Styles {
  [key: string]: string
}

const css: Styles = {
  border: '1px solid black',
  marginTop: '2px',
  borderRadius: '5px'
}

/**Наследование */
interface HiddenCircle extends Circle {
  hidden: boolean;
  setHidden: (value: boolean) => void
}

const hiddenCircle: HiddenCircle = {
  coords: {
    x: 0,
    y: 0,
  },
  hidden: false,
  radius: 1,
  color: 'red',
  setHidden: (value: boolean) => this.hidden = value
}

/**Реализация */

type PetType = 'cat' | 'dog';

interface IPet {
  type: PetType;
  name: string;
  setName: (value: string) => void;
  attached: boolean;
  setAttached: (value: boolean) => void
}

class Pet implements IPet {
  type: PetType = 'cat';
  name = 'Mushka'
  attached = true

  setName(value: string) {
    this.name = value;
  }

  setAttached(value: boolean) {
    this.attached = value;
  }
}
